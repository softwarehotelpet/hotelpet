/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techcats.softwarepethotel.DAO;

import com.techcats.softwarepethotel.Model.Pessoa;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author a1502549
 */
public class PessoaDAO extends DAO {

    @Override
    public List <Pessoa> findAll() {
         return entityManager.createQuery("FROM Pessoa").getResultList();
    }

    @Override
    public Object getById(int id) {
        return entityManager.find(Pessoa.class, id);
    }

    @Override
    public boolean removeById(int id) {
        boolean result = true;

        try {
            Pessoa pessoa = (Pessoa) this.getById(id);
            super.remove(pessoa);
        } catch (Exception ex) {
            ex.printStackTrace();
            result = false;
        }

        return result;
    }
    
     public boolean listar(){
        boolean result = false;
        
        Query query = entityManager.createQuery(
                "FROM Pessoa");
        
        if (query.getResultList().size() > 0) {
            result = true;
            System.out.println("teste");
        }

        return result;
    }
    
}
