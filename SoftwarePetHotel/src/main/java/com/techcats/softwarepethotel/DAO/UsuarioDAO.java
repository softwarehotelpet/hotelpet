/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techcats.softwarepethotel.DAO;

import com.techcats.softwarepethotel.Model.Usuario;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author CLEYTON
 */
public class UsuarioDAO extends DAO {

    @Override
    public List<Usuario> findAll() {
        return entityManager.createQuery("FROM Usuario").getResultList();
    }

    @Override
    public Usuario getById(int id) {
        return entityManager.find(Usuario.class, id);
    }

    @Override
    public boolean removeById(int id) {
        boolean result = true;

        try {
            Usuario usuario = this.getById(id);
            super.remove(usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
            result = false;
        }

        return result;
    }

    public boolean logar(Usuario usuario) {

        boolean result = false;

        Query query = entityManager.createQuery(
                "FROM Usuario WHERE nomeUsuario = :usuario AND senhaUsuario = :senha");

        query.setParameter("usuario", usuario.getNomeUsuario());
        query.setParameter("senha", usuario.getSenhaUsuario());

        if (query.getResultList().size() == 1) {
            result = true;
        }

        return result;

    }
    
    public List<Usuario> listarParcial(String parte) {

        Query query = entityManager.createQuery(
                "FROM Usuario WHERE nomeUsuario LIKE '%"+ parte +"%' OR nomeCompleto LIKE '%"+ parte +"%'");

        return query.getResultList();
    }
}
