/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techcats.softwarepethotel.DAO;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author CLEYTON
 */
public abstract class DAO<E> {

    protected static EntityManager entityManager = DAO.getEntityManager();

    
    public DAO() {
        //entityManager = getEntityManager();
    }

    private static EntityManager getEntityManager() {

        EntityManager entityManager = DAO.entityManager;
        
        if (entityManager == null) {
        
            EntityManagerFactory factory
                = Persistence.createEntityManagerFactory("hibernate-example");

            entityManager = factory.createEntityManager();
        }

        return entityManager;
    }

    public abstract List<E> findAll();

    public boolean persist(E object) {

        boolean result = true;

        try {
            entityManager.getTransaction().begin();
            entityManager.persist(object);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            entityManager.getTransaction().rollback();
            result = false;
        }

        return result;
    }

    public boolean merge(E object) {

        boolean result = true;

        try {
            entityManager.getTransaction().begin();
            entityManager.merge(object);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
            result = false;
        }

        return result;
    }

    public boolean remove(E object) {

        boolean result = true;

        try {
            entityManager.getTransaction().begin();
            entityManager.remove(object);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
            result = false;
        }

        return result;
    }

    public abstract E getById(final int id);

    public abstract boolean removeById(final int id);
}
