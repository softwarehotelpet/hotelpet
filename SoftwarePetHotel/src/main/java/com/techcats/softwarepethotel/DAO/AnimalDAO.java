/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techcats.softwarepethotel.DAO;

import com.techcats.softwarepethotel.Model.Animal;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author a1502549
 */
public class AnimalDAO extends DAO {

    @Override
    public List<Animal> findAll() {
        return entityManager.createQuery("FROM Animal").getResultList();
    }

    @Override
    public Object getById(int id) {
        return entityManager.find(Animal.class, id);
    }

    @Override
    public boolean removeById(int id) {
         boolean result = true;

        try {
            Animal animal = (Animal) this.getById(id);
            super.remove(animal);
        } catch (Exception ex) {
            ex.printStackTrace();
            result = false;
        }

        return result;
    }
    
     public boolean listar(){
        boolean result = false;
        
        Query query = entityManager.createQuery(
                "FROM Animal");
        
        if (query.getResultList().size() > 0) {
            result = true;
            System.out.println("teste");
        }

        return result;
    }

}
