/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techcats.softwarepethotel.Model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author a1502727
 */
@Entity
@Table
public class Estado implements Serializable  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer idEstado;
    
    @Column
    @OneToMany(
            mappedBy = "estado", 
            targetEntity = Cidade.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
        )
    private List<Cidade> cidade;
    
    @Column(nullable = false, length = 2)
    private String uf;
    
    @Column(nullable = false, length = 30)
    private String nomeEstado;

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public List<Cidade> getListaCidade() {
        return cidade;
    }

    public void setCidade(List<Cidade> cidade) {
        this.cidade = cidade;
    }

    public String getNomeEstado() {
        return nomeEstado;
    }

    public void setNomeEstado(String nomeEstado) {
        this.nomeEstado = nomeEstado;
    }

    public void addCidade(Cidade cidade) {
        cidade.setEstado(this);
        this.cidade.add(cidade);
    }

    public void removeCidade(Telefone cidade) {
        this.cidade.remove(cidade);
    }

    public void getCidade(int index) {
        this.cidade.get(index);
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }
    
    
}
