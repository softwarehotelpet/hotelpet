/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techcats.softwarepethotel.Model;

import com.techcats.softwarepethotel.ENUM.SimNao;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 *
 * @author a1502727
 */
@Entity
public class Saude implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer idSaude;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="idAnimal", nullable = false)
    private Animal animal;
    
    @Column (nullable = false)
    private SimNao vermifugo;
    
    @Column (length = 10)
    private String vermifugoData;
    
    @Column (nullable = false)
    private SimNao antipugas;
    
    @Column(length = 10)
    private String antipugasData;
    
    @Column (nullable = false)
    private SimNao vacinaV10;
    
    @Column(length = 10)
    private String vacinaV10Data;
    
    @Column (nullable = false)
    private SimNao vacinaBronchi;
    
    @Column(length = 10)
    private String vacinaBronchiData;
    
    @Column  (nullable = false)
    private SimNao vacinaRaiva;
    
    @Column(length = 10)
    private String vacinaRaivaData;
    
    @Column (nullable = false)
    private SimNao vacinaGiardia;
    
    @Column(length = 10)
    private String vacinaGiardiaData;
    
    @Column(columnDefinition = "TEXT")
    private String observacoes;

    public Integer getIdSaude() {
        return idSaude;
    }

    public void setIdSaude(Integer idSaude) {
        this.idSaude = idSaude;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public SimNao getVermifugo() {
        return vermifugo;
    }

    public void setVermifugo(SimNao vermifugo) {
        this.vermifugo = vermifugo;
    }

    public String getVermifugoData() {
        return vermifugoData;
    }

    public void setVermifugoData(String vermifugoData) {
        this.vermifugoData = vermifugoData;
    }

    public SimNao getAntipugas() {
        return antipugas;
    }

    public void setAntipugas(SimNao antipugas) {
        this.antipugas = antipugas;
    }

    public String getAntipugasData() {
        return antipugasData;
    }

    public void setAntipugasData(String antipugasData) {
        this.antipugasData = antipugasData;
    }

    public SimNao getVacinaV10() {
        return vacinaV10;
    }

    public void setVacinaV10(SimNao vacinaV10) {
        this.vacinaV10 = vacinaV10;
    }

    public String getVacinaV10Data() {
        return vacinaV10Data;
    }

    public void setVacinaV10Data(String vacinaV10Data) {
        this.vacinaV10Data = vacinaV10Data;
    }

    public SimNao getVacinaBronchi() {
        return vacinaBronchi;
    }

    public void setVacinaBronchi(SimNao vacinaBronchi) {
        this.vacinaBronchi = vacinaBronchi;
    }

    public String getVacinaBronchiData() {
        return vacinaBronchiData;
    }

    public void setVacinaBronchiData(String vacinaBronchiData) {
        this.vacinaBronchiData = vacinaBronchiData;
    }

    public SimNao getVacinaRaiva() {
        return vacinaRaiva;
    }

    public void setVacinaRaiva(SimNao vacinaRaiva) {
        this.vacinaRaiva = vacinaRaiva;
    }

    public String getVacinaRaivaData() {
        return vacinaRaivaData;
    }

    public void setVacinaRaivaData(String vacinaRaivaData) {
        this.vacinaRaivaData = vacinaRaivaData;
    }

    public SimNao getVacinaGiardia() {
        return vacinaGiardia;
    }

    public void setVacinaGiardia(SimNao vacinaGiardia) {
        this.vacinaGiardia = vacinaGiardia;
    }

    public String getVacinaGiardiaData() {
        return vacinaGiardiaData;
    }

    public void setVacinaGiardiaData(String vacinaGiardiaData) {
        this.vacinaGiardiaData = vacinaGiardiaData;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }
    
    
}
