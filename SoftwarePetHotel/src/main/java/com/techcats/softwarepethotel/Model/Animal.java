package com.techcats.softwarepethotel.Model;

import com.techcats.softwarepethotel.ENUM.Porte;
import com.techcats.softwarepethotel.ENUM.Sexo;
import com.techcats.softwarepethotel.ENUM.SimNao;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.Inheritance;
//import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author Nikolas
 */

@Entity
//@Inheritance(strategy = InheritanceType.JOINED)
public class Animal implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer idAnimal;
    
    @Column(nullable = false, length = 50)
    private String nome;
    
    @Column(nullable = false, length = 25)
    private String raca;
    
    @Column(nullable = false)
    private Porte porte;
    
    @Column(nullable = false)
    private Sexo sexo;
    
    @Column(nullable = false, length = 30)
    private String cor;
    
    @Column(nullable = false)
    private float peso;

    @Column(nullable = false)
    private String dataNascimento;
   
    @Column(nullable = false)
    private SimNao castrado;
    
    @Column(nullable = true)
    private SimNao chip;
    
    @Column(nullable = false, length = 20)
    private String numeroChip;
    
    @Column(nullable = false)
    private String observacao;
    
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="idPessoa", nullable = false)
    private Proprietario proprietario;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "animal", targetEntity = Veterinario.class, cascade = CascadeType.ALL)
    private Veterinario veterinario;
    
    @OneToOne(fetch = FetchType.EAGER, mappedBy = "animal", targetEntity = Saude.class, cascade = CascadeType.ALL)
    private Saude saude;
    
    @OneToOne(fetch = FetchType.EAGER, mappedBy = "animal", targetEntity = Alimentacao.class, cascade = CascadeType.ALL)
    private Alimentacao alimentacao;
    
    @OneToOne(fetch = FetchType.EAGER, mappedBy = "animal", targetEntity = Habito.class, cascade = CascadeType.ALL)
    private Habito habito;

    public Integer getIdAnimal() {
        return idAnimal;
    }

    public void setIdAnimal(Integer idAnimal) {
        this.idAnimal = idAnimal;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRaca() {
        return raca;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }

    public Porte getPorte() {
        return porte;
    }

    public void setPorte(Porte porte) {
        this.porte = porte;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public SimNao getCastrado() {
        return castrado;
    }

    public void setCastrado(SimNao castrado) {
        this.castrado = castrado;
    }

    public SimNao getChip() {
        return chip;
    }

    public void setChip(SimNao chip) {
        this.chip = chip;
    }

    public String getNumeroChip() {
        return numeroChip;
    }

    public void setNumeroChip(String numeroChip) {
        this.numeroChip = numeroChip;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Proprietario getProprietario() {
        return proprietario;
    }

    public void setProprietario(Proprietario proprietario) {
        proprietario.addAnimal(this);
        this.proprietario = proprietario;
    }

    public Veterinario getVeterinario() {
        return veterinario;
    }

    public void setVeterinario(Veterinario veterinario) {
        veterinario.setAnimal(this);
        this.veterinario = veterinario;
    }

    public Saude getSaude() {
        return saude;
    }

    public void setSaude(Saude saude) {
        saude.setAnimal(this);
        this.saude = saude;
    }

    public Alimentacao getAlimentacao() {
        return alimentacao;
    }

    public void setAlimentacao(Alimentacao alimentacao) {
        alimentacao.setAnimal(this);
        this.alimentacao = alimentacao;
    }

    public Habito getHabito() {
        return habito;
    }

    public void setHabito(Habito habito) {
        habito.setAnimal(this);
        this.habito = habito;
    }
}
