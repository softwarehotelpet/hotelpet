/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techcats.softwarepethotel.Model;

import com.techcats.softwarepethotel.ENUM.Agua;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 *
 * @author a1502727
 */
@Entity
public class Alimentacao implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer idAlimentacao;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="idAnimal", nullable = false)
    private Animal animal;
    
    @Column(nullable = false, length = 100)
    private String racao;

    @Column(nullable = false)
    private boolean comida;

    @Column(nullable = false)
    private boolean petiscos;

    @Column(nullable = false)
    private boolean ossinhos;

    @Column(nullable = false)
    private Agua agua;

    public Integer getIdAlimentacao() {
        return idAlimentacao;
    }

    public void setIdAlimentacao(Integer idAlimentacao) {
        this.idAlimentacao = idAlimentacao;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public String getRacao() {
        return racao;
    }

    public void setRacao(String racao) {
        this.racao = racao;
    }

    public boolean isComida() {
        return comida;
    }

    public void setComida(boolean comida) {
        this.comida = comida;
    }

    public boolean isPetiscos() {
        return petiscos;
    }

    public void setPetiscos(boolean petiscos) {
        this.petiscos = petiscos;
    }

    public boolean isOssinhos() {
        return ossinhos;
    }

    public void setOssinhos(boolean ossinhos) {
        this.ossinhos = ossinhos;
    }

    public Agua getAgua() {
        return agua;
    }

    public void setAgua(Agua agua) {
        this.agua = agua;
    }

    
}
