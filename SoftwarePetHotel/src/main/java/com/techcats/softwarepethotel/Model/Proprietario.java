/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techcats.softwarepethotel.Model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author cleyton
 */
@Entity
public class Proprietario extends Pessoa{

    @Column(nullable = false)
    private String endereco;
    
    @Column(nullable = false)
    private String CEP;
    
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="idCidade", nullable = false)
    private Cidade cidade;
    
    @Column(nullable = false)
    private String bairro;
    
    @Column
    private String complemento;
    
    @Column(nullable = false)
    private String numero;
    
    @Column
    @OneToMany(
            mappedBy = "proprietario", 
            targetEntity = Telefone.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
        )
    private final List<Telefone> telefones;
    
    @Column
    @OneToMany(
            mappedBy = "proprietario", 
            targetEntity = Animal.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
        )
    private final List<Animal> animais;
    
    public Proprietario() {
        telefones = new ArrayList<>();
        animais = new ArrayList<>();
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCEP() {
        return CEP;
    }

    public void setCEP(String CEP) {
        this.CEP = CEP;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void addTelefone(Telefone telefone) {
        telefone.setProprietario(this);
        this.telefones.add(telefone);
    }

    public void removeTelefone(Telefone telefone) {
        this.telefones.remove(telefone);
    }

    public Telefone getTelefone(int index) {
        return this.telefones.get(index);
    }

    public List<Telefone> getTelefones() {
        return telefones;
    }
    
    public void addAnimal(Animal animal) {
        this.animais.add(animal);
    }

    public void removeAnimal(Animal animal) {
        this.telefones.remove(animal);
    }

    public Animal getAnimal(int index) {
        return this.animais.get(index);
    }

    public List<Animal> getAnimais() {
        return this.animais;
    }
}
