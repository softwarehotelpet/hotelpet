/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techcats.softwarepethotel.Model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author a1502549
 */
@Entity
public class Hospedagem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer idHospedagem;

    @Column(nullable = false)
    private Date dataEntrada;

    @Column(nullable = false)
    private Date dataSaida;

    @Column(nullable = false)
    private float valorDiaria;

    @Column(nullable = false)
    private float descontos;

    @Column(nullable = false)
    private boolean pagamento;

    @Column(nullable = false)
    private float total;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "idPessoa", nullable = false)
    private Proprietario nomeProprietario;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "idAnimal", nullable = false)
    private Animal nomeAnimal;
}
