/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techcats.softwarepethotel.Model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 *
 * @author a1502727
 */
@Entity
public class Habito implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer idHabito;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idAnimal", nullable = false)
    private Animal animal;

    @Column(nullable = true)
    private boolean agressivoPessoas;

    @Column(nullable = true)
    private boolean agressivoCaes;

    @Column(nullable = true)
    private boolean agressivoDominante;

    @Column(nullable = true)
    private boolean agressivoMedo;

    @Column(nullable = true)
    private boolean hiperativo;

    @Column(nullable = true)
    private boolean ansiedadeSozinho;

    @Column(nullable = true)
    private boolean ansiedadePreso;

    @Column(nullable = true)
    private boolean fobiaChuva;

    @Column(nullable = true)
    private boolean fobiaTrovao;

    @Column(nullable = true)
    private boolean fobiaFogos;

    @Column(nullable = true)
    private boolean fobiaAgua;

    @Column(nullable = true)
    private boolean AutoestimaSub;

    @Column(nullable = true)
    private boolean AutoestimaPoucaEnergia;

    @Column(nullable = true)
    private boolean AutoestimaMenteFraca;

    @Column(nullable = true)
    private boolean AutoestimaMedo;

    @Column(nullable = true)
    private boolean DestruicaoObjetos;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String ObservacaoHabito;

    public Integer getIdHabito() {
        return idHabito;
    }

    public void setIdHabito(Integer idHabito) {
        this.idHabito = idHabito;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public boolean isAgressivoPessoas() {
        return agressivoPessoas;
    }

    public void setAgressivoPessoas(boolean agressivoPessoas) {
        this.agressivoPessoas = agressivoPessoas;
    }

    public boolean isAgressivoCaes() {
        return agressivoCaes;
    }

    public void setAgressivoCaes(boolean agressivoCaes) {
        this.agressivoCaes = agressivoCaes;
    }

    public boolean isAgressivoDominante() {
        return agressivoDominante;
    }

    public void setAgressivoDominante(boolean agressivoDominante) {
        this.agressivoDominante = agressivoDominante;
    }

    public boolean isAgressivoMedo() {
        return agressivoMedo;
    }

    public void setAgressivoMedo(boolean agressivoMedo) {
        this.agressivoMedo = agressivoMedo;
    }

    public boolean isHiperativo() {
        return hiperativo;
    }

    public void setHiperativo(boolean hiperativo) {
        this.hiperativo = hiperativo;
    }

    public boolean isAnsiedadeSozinho() {
        return ansiedadeSozinho;
    }

    public void setAnsiedadeSozinho(boolean ansiedadeSozinho) {
        this.ansiedadeSozinho = ansiedadeSozinho;
    }

    public boolean isAnsiedadePreso() {
        return ansiedadePreso;
    }

    public void setAnsiedadePreso(boolean ansiedadePreso) {
        this.ansiedadePreso = ansiedadePreso;
    }

    public boolean isFobiaChuva() {
        return fobiaChuva;
    }

    public void setFobiaChuva(boolean fobiaChuva) {
        this.fobiaChuva = fobiaChuva;
    }

    public boolean isFobiaTrovao() {
        return fobiaTrovao;
    }

    public void setFobiaTrovao(boolean fobiaTrovao) {
        this.fobiaTrovao = fobiaTrovao;
    }

    public boolean isFobiaFogos() {
        return fobiaFogos;
    }

    public void setFobiaFogos(boolean fobiaFogos) {
        this.fobiaFogos = fobiaFogos;
    }

    public boolean isFobiaAgua() {
        return fobiaAgua;
    }

    public void setFobiaAgua(boolean fobiaAgua) {
        this.fobiaAgua = fobiaAgua;
    }

    public boolean isAutoestimaSub() {
        return AutoestimaSub;
    }

    public void setAutoestimaSub(boolean AutoestimaSub) {
        this.AutoestimaSub = AutoestimaSub;
    }

    public boolean isAutoestimaPoucaEnergia() {
        return AutoestimaPoucaEnergia;
    }

    public void setAutoestimaPoucaEnergia(boolean AutoestimaPoucaEnergia) {
        this.AutoestimaPoucaEnergia = AutoestimaPoucaEnergia;
    }

    public boolean isAutoestimaMenteFraca() {
        return AutoestimaMenteFraca;
    }

    public void setAutoestimaMenteFraca(boolean AutoestimaMenteFraca) {
        this.AutoestimaMenteFraca = AutoestimaMenteFraca;
    }

    public boolean isAutoestimaMedo() {
        return AutoestimaMedo;
    }

    public void setAutoestimaMedo(boolean AutoestimaMedo) {
        this.AutoestimaMedo = AutoestimaMedo;
    }

    public boolean isDestruicaoObjetos() {
        return DestruicaoObjetos;
    }

    public void setDestruicaoObjetos(boolean DestruicaoObjetos) {
        this.DestruicaoObjetos = DestruicaoObjetos;
    }

    public String getObservacaoHabito() {
        return ObservacaoHabito;
    }

    public void setObservacaoHabito(String ObservacaoHabito) {
        this.ObservacaoHabito = ObservacaoHabito;
    }

}
