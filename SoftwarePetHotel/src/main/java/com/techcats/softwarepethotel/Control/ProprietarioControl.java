/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techcats.softwarepethotel.Control;

import com.techcats.softwarepethotel.DAO.EstadoDAO;
import com.techcats.softwarepethotel.DAO.ProprietarioDAO;
import com.techcats.softwarepethotel.Model.Cidade;
import com.techcats.softwarepethotel.Model.Estado;
import com.techcats.softwarepethotel.Model.Proprietario;
import com.techcats.softwarepethotel.View.jMain;
import com.techcats.softwarepethotel.View.jProprietario;
import com.techcats.softwarepethotel.View.jProprietarioListagem2;
import com.techcats.softwarepethotel.View.jProprietarioListagem;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author a1502549
 */
public class ProprietarioControl {

    private ProprietarioDAO dao;
    private EstadoDAO estadoDAO;
    private Proprietario proprietario;
    private List<Estado> estados;
    private List<Proprietario> proprietarios;

    public ProprietarioControl() {
        this.dao = new ProprietarioDAO();
        this.estadoDAO = new EstadoDAO();
    }

    public void setUsuario(Proprietario proprietario) {
        this.proprietario = proprietario;
    }

    public void cadastrar() {
        if (dao.persist(this.proprietario)) {
            JOptionPane.showMessageDialog(null, "Proprietario cadastrado com sucesso!",
                    "Sucesso...", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Erro no cadastro do proprietario!",
                    "Atenção...", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void editar() {
        if (dao.merge(this.proprietario)) {
            JOptionPane.showMessageDialog(null, "Proprietario alterado com sucesso!",
                    "Sucesso...", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Erro na alteração do proprietario!",
                    "Atenção...", JOptionPane.WARNING_MESSAGE);
        }
    }
    
    public void remover(jProprietarioListagem listagem, int index){
        if (dao.removeById(proprietarios.get(index).getIdPessoa())) {
            JOptionPane.showMessageDialog(null, "Proprietário removido com sucesso!",
                    "Sucesso...", JOptionPane.INFORMATION_MESSAGE);
            this.listarTudo(listagem);
        } else {
            JOptionPane.showMessageDialog(null, "Erro na remoção do Proprietário!",
                    "Atenção...", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void carregarEstados(jProprietario p) {
        this.estados = estadoDAO.findAll();

        if (!this.estados.isEmpty()) {
            for (Estado estado : this.estados) {
                p.JCmbEstado.addItem(estado.getNomeEstado());
            }
        }
    }

    public void carregarCidade(jProprietario p, int index) {
        p.JCmbCidade.removeAllItems();
        p.JCmbCidade.addItem("Escolha uma cidade...");

        if (!this.estados.isEmpty()) {
            for (Cidade cidade : this.estados.get(index - 1).getListaCidade()) {
                p.JCmbCidade.addItem(cidade.getNomeCidade());
            }
        }

    }

    public Cidade recuperarCidade(int indexEstado, int indexCidade) {
        return this.estados.get(indexEstado - 1).getListaCidade().get(indexCidade - 1);
    }

    public void listarTudo(jProprietarioListagem listagem) {
        proprietarios = dao.findAll();

        while (listagem.modelo.getRowCount() > 0) {
            listagem.modelo.removeRow(0);
        }

        if (!proprietarios.isEmpty()) {
            for (Proprietario user : proprietarios) {
                listagem.modelo.addRow(new Object[]{
                    user.getNomeCompleto(),
                    user.getCPF()
                });
            }
        }
    }
    
    public void listarParcial(jProprietarioListagem listagem, String parte) {
        proprietarios = dao.listarParcial(parte);

        while (listagem.modelo.getRowCount() > 0) {
            listagem.modelo.removeRow(0);
        }

        if (!proprietarios.isEmpty()) {
            for (Proprietario user : proprietarios) {
                listagem.modelo.addRow(new Object[]{
                    user.getNomeCompleto(),
                    user.getCPF()
                });
            }
        }
    }
    
   public void listarTudo2(jProprietarioListagem2 listagem) {
        proprietarios = dao.findAll();

        listagem.modelo.removeAllElements();

        if (!proprietarios.isEmpty()) {
            for (Proprietario p : proprietarios) {
                listagem.modelo.addElement(p.getNomeCompleto() + " " + p.getCPF());
            }
        }
    }

     public void listarParcial2(jProprietarioListagem2 listagem, String parte) {
        proprietarios = dao.listarParcial(parte);

        listagem.modelo.removeAllElements();

        if (!proprietarios.isEmpty()) {
            for (Proprietario p : proprietarios) {
                listagem.modelo.addElement(p.getNomeCompleto() + " " + p.getCPF());
            }
        }
    }
     
     public Proprietario recuperarProprietario(int index) {
        return this.proprietarios.get(index);
    }
    
    public void abrirEdicao(int index){
        jProprietario usuario = new jProprietario(proprietarios.get(index));
        jMain.jDesktopPane1.add(usuario);
        usuario.setVisible(true);
    }
}
