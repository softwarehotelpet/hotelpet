/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techcats.softwarepethotel.Control;

import com.techcats.softwarepethotel.DAO.AnimalDAO;
import com.techcats.softwarepethotel.Model.Animal;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author a1502549
 */
public class AnimalControl {

    private AnimalDAO dao;
    private Animal animal;
    private List<Animal> animais;

    public AnimalControl() {
        this.dao = new AnimalDAO();
    }

    public AnimalControl(Animal animal) {
        this();
        this.animal = animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public boolean cadastrar() {
        boolean result = false;
        if (dao.persist(this.animal)) {
            JOptionPane.showMessageDialog(null, "Animal cadastrado com sucesso!",
                    "Sucesso...", JOptionPane.INFORMATION_MESSAGE);
            result = true;
        } else {
            JOptionPane.showMessageDialog(null, "Erro no cadastro do animal!",
                    "Atenção...", JOptionPane.WARNING_MESSAGE);
        }
        return result;
    }

    
}
