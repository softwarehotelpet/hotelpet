/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techcats.softwarepethotel.Control;

import com.techcats.softwarepethotel.DAO.PessoaDAO;
import com.techcats.softwarepethotel.Model.Pessoa;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author a1502549
 */
public class PessoaControl {

    private PessoaDAO dao;
    private Pessoa pessoa;
    private List<Pessoa> pessoas;

    public PessoaControl() {
        this.dao = new PessoaDAO();
    }

    public PessoaControl(Pessoa pessoa) {
        this();
        this.pessoa = pessoa;
    }

    public void setUsuario(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

//    public void cadastrar() {
//        if (dao.persist(this.pessoa)) {
//            JOptionPane.showMessageDialog(null, "Pessoa cadastrada com sucesso!",
//                    "Sucesso...", JOptionPane.INFORMATION_MESSAGE);
//        } else {
//            JOptionPane.showMessageDialog(null, "Erro no cadastro da pessoa!",
//                    "Atenção...", JOptionPane.WARNING_MESSAGE);
//        }
//    }

    public void editar() {
        if (dao.merge(this.pessoa)) {
            JOptionPane.showMessageDialog(null, "Pessoa alterada com sucesso!",
                    "Sucesso...", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Erro na alteração da pessoa!",
                    "Atenção...", JOptionPane.WARNING_MESSAGE);
        }
    }

}
