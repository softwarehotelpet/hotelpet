/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techcats.softwarepethotel.Control;

import com.techcats.softwarepethotel.DAO.UsuarioDAO;
import com.techcats.softwarepethotel.Model.Usuario;
import com.techcats.softwarepethotel.View.jLogin;
import com.techcats.softwarepethotel.View.jMain;
import com.techcats.softwarepethotel.View.jProprietario;
import com.techcats.softwarepethotel.View.jUsuario;
import com.techcats.softwarepethotel.View.jUsuarioEditar;
import com.techcats.softwarepethotel.View.jUsuarioListagem;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author CLEYTON
 */
public class UsuarioControl {

    private UsuarioDAO dao;
    private Usuario usuario;
    private List<Usuario> usuarios;

    public UsuarioControl() {
        this.dao = new UsuarioDAO();
    }

    public UsuarioControl(Usuario usuario) {
        this();
        this.usuario = usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public void logar(jLogin login) {
        if (dao.logar(this.usuario)) {
            jMain principal = new jMain();
            principal.setVisible(true);
            login.dispose();
        } else {
            JOptionPane.showMessageDialog(null, "Usuário ou senha incorretos! \n"
                    + "Por favor verifique seus dados e tente novamente.", "Atenção...", JOptionPane.WARNING_MESSAGE);
        }
    }

    public boolean cadastrar() {
        boolean resultado = false;
        if (dao.persist(this.usuario)) {
            JOptionPane.showMessageDialog(null, "Usuário cadastrado com sucesso!",
                    "Sucesso...", JOptionPane.INFORMATION_MESSAGE);
            resultado = true;
        } else {
            JOptionPane.showMessageDialog(null, "Erro no cadastro do usuário!",
                    "Atenção...", JOptionPane.WARNING_MESSAGE);
        }
        return resultado;
    }
    
    public void editar(){
        if (dao.merge(this.usuario)) {
            JOptionPane.showMessageDialog(null, "Usuário alterado com sucesso!",
                    "Sucesso...", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Erro na alteração do usuário!",
                    "Atenção...", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void remover(jUsuarioListagem listagem, int index){
        if (dao.removeById(usuarios.get(index).getIdPessoa())) {
            JOptionPane.showMessageDialog(null, "Usuário removido com sucesso!",
                    "Sucesso...", JOptionPane.INFORMATION_MESSAGE);
            this.listarTudo(listagem);
        } else {
            JOptionPane.showMessageDialog(null, "Erro na remoção do usuário!",
                    "Atenção...", JOptionPane.WARNING_MESSAGE);
        }
    }
    
    public void listarTudo(jUsuarioListagem listagem) {
        usuarios = dao.findAll();
        
        while(listagem.modelo.getRowCount()>0)
            listagem.modelo.removeRow(0);
        
        
        if(!usuarios.isEmpty()){
            for(Usuario user : usuarios){
                listagem.modelo.addRow(new Object[]{
                    user.getNomeCompleto(), 
                    user.getNomeUsuario(),
                    user.getEmail()
                });
             }
        }
    }
    
    public void listarParcial(jUsuarioListagem listagem, String parte) {
        usuarios = dao.listarParcial(parte);
        
        while(listagem.modelo.getRowCount()>0)
            listagem.modelo.removeRow(0);
        
        if(!usuarios.isEmpty()){
            for(Usuario user : usuarios){
                listagem.modelo.addRow(new Object[]{
                    user.getNomeCompleto(), 
                    user.getNomeUsuario(),
                    user.getEmail()
                });
             }
        }
    }
    
    public void abrirEdicao(int index){
        jUsuarioEditar usuario = new jUsuarioEditar(usuarios.get(index));
        jMain.jDesktopPane1.add(usuario);
        usuario.setVisible(true);
    }
}
