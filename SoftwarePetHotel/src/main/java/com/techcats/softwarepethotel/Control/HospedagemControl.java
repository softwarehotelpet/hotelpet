/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techcats.softwarepethotel.Control;

import com.techcats.softwarepethotel.DAO.HospedagemDAO;
import com.techcats.softwarepethotel.Model.Hospedagem;
import java.util.List;

/**
 *
 * @author a1502549
 */
public class HospedagemControl {

    private HospedagemDAO dao;
    private Hospedagem hospedagem;
    private List<Hospedagem> hospedagens;

    public HospedagemControl() {
        this.dao = new HospedagemDAO();
    }

    public HospedagemControl(Hospedagem hospedagem) {
        this();
        this.hospedagem = hospedagem;
    }

    public void setUsuario(Hospedagem hospedagem) {
        this.hospedagem = hospedagem;
    }
}
