/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techcats.softwarepethotel.Control;

import com.techcats.softwarepethotel.DAO.TelefoneDAO;
import com.techcats.softwarepethotel.Model.Telefone;
import com.techcats.softwarepethotel.View.jProprietario;
import com.techcats.softwarepethotel.View.jTelefone;
import java.io.PrintStream;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author a1502549
 */
public class TelefoneControl {

    private TelefoneDAO dao;
    private Telefone telefone;
    private List<Telefone> telefones;

    public TelefoneControl() {
        this.dao = new TelefoneDAO();
    }

    public TelefoneControl(Telefone telefone) {
        this();
        this.telefone = telefone;
    }

    public void setUsuario(Telefone telefone) {
        this.telefone = telefone;
    }

    public void cadastrar() {
        if (dao.persist(this.telefone)) {
            JOptionPane.showMessageDialog(null, "Telefone cadastrado com sucesso!",
                    "Sucesso...", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Erro no cadastro do Telefone!",
                    "Atenção...", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void editar() {
        if (dao.merge(this.telefone)) {
            JOptionPane.showMessageDialog(null, "Telefone alterado com sucesso!",
                    "Sucesso...", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Erro na alteração do Telefone!",
                    "Atenção...", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void listarTudo(jProprietario listagem) {
        telefones = dao.findAll();

        if (!telefones.isEmpty()) {
            for (Telefone user : telefones) {
                listagem.modelo.addRow(new Object[]{
                    user.getDdd(),
                    user.getNumero()
                });
            }
        }
    }
}
