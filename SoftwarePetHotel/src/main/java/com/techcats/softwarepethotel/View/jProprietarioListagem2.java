/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techcats.softwarepethotel.View;

import com.techcats.softwarepethotel.Control.ProprietarioControl;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 *
 * @author CLEYTON
 */
public class jProprietarioListagem2 extends javax.swing.JInternalFrame {

    private ProprietarioControl control = new ProprietarioControl();
    public DefaultListModel modelo = new DefaultListModel();
    private jAnimal animal;

    /**
     * Creates new form jProprietarioAnimalListagem
     */
    public jProprietarioListagem2(jAnimal animal) {
        initComponents();
        int lDesk = jMain.jDesktopPane1.getWidth();
        int aDesk = jMain.jDesktopPane1.getHeight();
        int lIFrame = this.getWidth();
        int aIFrame = this.getHeight();
        this.setLocation(lDesk / 2 - lIFrame / 2, aDesk / 2 - aIFrame / 2);

        jLstProprietarios.setModel(modelo);
        control.listarTudo2(this);

        this.animal = animal;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jLstProprietarios = new javax.swing.JList<>();
        jTxtNomeProprietario = new javax.swing.JTextField();
        jBtnIncluir = new javax.swing.JButton();

        setClosable(true);

        jLstProprietarios.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jLstProprietarios);

        jTxtNomeProprietario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtNomeProprietarioKeyPressed(evt);
            }
        });

        jBtnIncluir.setText("INCLUIR");
        jBtnIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnIncluirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTxtNomeProprietario)
                    .addComponent(jBtnIncluir, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 374, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jTxtNomeProprietario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBtnIncluir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnIncluirActionPerformed
        if(jLstProprietarios.getSelectedIndex()<0){
            JOptionPane.showMessageDialog(null, "Selecione um proprietário", "Atenção...", JOptionPane.WARNING_MESSAGE);
        }else{
            animal.setProprietario(control.recuperarProprietario(jLstProprietarios.getSelectedIndex()));
            this.dispose();
        }
    }//GEN-LAST:event_jBtnIncluirActionPerformed

    private void jTxtNomeProprietarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtNomeProprietarioKeyPressed
        if (!jTxtNomeProprietario.getText().isEmpty()) {
            control.listarParcial2(this, jTxtNomeProprietario.getText());
        }else{
            control.listarTudo2(this);
        }
    }//GEN-LAST:event_jTxtNomeProprietarioKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnIncluir;
    public javax.swing.JList<String> jLstProprietarios;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTxtNomeProprietario;
    // End of variables declaration//GEN-END:variables
}
